import paho.mqtt.client as mqtt

# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, rc):
    print("Connected with result code "+str(rc))
    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe("sensenet/ALERTTECH/AT0603/#")

# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    #needed to add the 'utf-8' to get rid of the 'b' in front of the payload.
    print(msg.topic+" "+msg.payload.decode())
    with open("newfile.txt", "ab") as myfile:
        #myfile.write(str(msg.topic+" "+msg.payload.decode()))
        myfile.write(msg.topic.encode('utf-8'))
        myfile.write(':'.encode('utf-8'))
        myfile.write(msg.payload)
        myfile.write('\n'.encode('utf-8'))
client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

client.connect("hostname/ip", 1883, 60)

# Blocking call that processes network traffic, dispatches callbacks and
# handles reconnecting.
client.loop_forever()